<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
%>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("status")
end %>

<%
local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Config"}), page_info)
	htmlviewfunctions.displayformstart(form, page_info)
local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="General Parameters"}), page_info, htmlviewfunctions.incrementheader(header_level))
	htmlviewfunctions.displayformitem(form.value.interface, "interface")
	htmlviewfunctions.displayformitem(form.value.listen_address, "listen_address")
	htmlviewfunctions.displayformitem(form.value.domain, "domain")
htmlviewfunctions.displaysectionend(header_level2)
htmlviewfunctions.displaysectionstart(cfe({label="DNS Settings"}), page_info, header_level2)
local header_level3 = htmlviewfunctions.displaysectionstart(cfe({label="DNS Parameters"}), page_info, htmlviewfunctions.incrementheader(header_level2))
	htmlviewfunctions.displayformitem(form.value.mx_host, "mx_host")
htmlviewfunctions.displaysectionend(header_level3)
htmlviewfunctions.displaysectionstart(cfe({label="DNS Filtering"}), page_info, header_level3)
	htmlviewfunctions.displayformitem(form.value.dns_filtering, "dns_filtering")
	htmlviewfunctions.displayformitem(form.value.dns_whitelist, "dns_whitelist")
	htmlviewfunctions.displayformitem(form.value.dns_default_address, "dns_default_address")
htmlviewfunctions.displaysectionend(header_level3)
htmlviewfunctions.displaysectionend(header_level2)
htmlviewfunctions.displaysectionstart(cfe({label="DHCP Parameters"}), page_info, header_level2)
	htmlviewfunctions.displayformitem(form.value.dhcp_range, "dhcp_range")
	htmlviewfunctions.displayformitem(form.value.no_dhcp_interface, "no_dhcp_interface")
	htmlviewfunctions.displayformitem(form.value.dhcp_host, "dhcp_host")
	htmlviewfunctions.displayformitem(form.value.dhcp_option, "dhcp_option")
htmlviewfunctions.displaysectionend(header_level2)
	htmlviewfunctions.displayformend(form, header_level2)
htmlviewfunctions.displaysectionend(header_level)
%>
